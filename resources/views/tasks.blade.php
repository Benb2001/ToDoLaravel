<!DOCTYPE HTML>
<html>
<head>
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
	<link rel="stylesheet" href="Styles/styles.css">
	<title>To-Do Application</title>
</head>
  <div class="container pt-5 h-100">
    <div class="row d-flex justify-content-center align-items-center h-100">
      <div class="col-10">
        <div class="card bg-info text-black" style="border-radius: 2rem;">
          <div class="card-body p-5">

            <div class="mt-4 pb-5">
            <?php
                
                //watch vid again
            ?>

              <h2 class="fw-bold mb-1 text-center"> {{session('username')}}'s To-Do List!</h2>
              <a href="/logout" >Logout</a>
              <p class="text-black mb-5 text-center">Create and view Tasks.</p>
              <form class="row mb-4" action="task" method="POST">
                @csrf
                <div class="text-center">
                    <div class="col-7 d-inline-block">
                        <input type="text" name="taskdesc" class="form-control" />
                    </div>   
                    <div class="col-4 mx-2 d-inline block">
                        <button type="submit" class="btn btn-success mb-1">Add Task</button>
                    </div>
                </div>    
              </form>
              
              <table class="table mb-4 table-hover">
              <thead>
                <tr>
                  <th>#</th>
                  <th>Tasks</th>
                  <th>Date</th>
                  <th>Completed?</th>
                  <th>Edit</th>
                  <th>Delete</th>
                  <th>Complete</th>
                </tr>
              
              </thead>
              @foreach($tasks as $task)<?php
              if($task['Username']==session('username')){              ?>
              <tbody>
                <tr>
                  <td>{{$task['TaskID']}}</th>
                  <td>{{$task['TaskDesc']}}</td>
                  <td>{{$task['Date']}}</td>
                  <td>{{$task['Completed']}}</td>
                  <?php $id = $task->TaskID; ?> 
                  <td>
                    <a href="{{ route('task.editview', $id) }}" class="btn btn-warning" >Edit Task</a>                  
                  </td>
                  <td>
                    <a href="{{ route('task.delete', $id) }}" class="btn btn-danger">Delete Task</a>
                  </td>
                  <td>
                    <a href="{{ route('task.finish', $id) }}" class="btn btn-success">Complete Task</a>
                  </td>
                </tr>
              </tbody>
              <?php }?>
              @endforeach
              
            </table>

              
            </div>

           </div>
        </div>
      </div>
    </div>
  </div>
</html>