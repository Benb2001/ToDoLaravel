<!DOCTYPE HTML>
<html>
<head>
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
	<link rel="stylesheet" href="Styles/styles.css">
	<title>Edit Task</title>
</head>
<div class="container pt-5 h-100">
    <div class="row d-flex justify-content-center align-items-center h-100">
      <div class="col-10">
        <div class="card bg-info text-black" style="border-radius: 2rem;">
          <div class="card-body p-5">

            <div class="mt-4 pb-5 text-center ">
                <?php   $data = Session::get('data');
                        $id = $data->TaskID;        
                
                ?>
              <h2 class="fw-bold mb-5">Edit Task</h2>
              
              <form action="{{ route('task.update', $id) }}" method="POST">
                @csrf
                <div class="mb-5 form-floating">                    
                    <input type="text" name="TaskID" id="txtTaskID" class="form-control" value="{{$data['TaskID']}}" readonly />   
                    <label class="form-label"  for="txtTaskID">Task ID:</label>                       
                </div>

                <div class="mb-5 form-floating">                    
                    <input type="text" name="TaskDesc" id="txtDesc" class="form-control" value="{{$data['TaskDesc']}}" />   
                    <label class="form-label"  for="txtDesc">Task Description:</label>                       
                </div>

                <button class="btn mb-3 btn-lg px-5 btn btn-success" type="submit">Update</button>
                <p class="lead fw-bold mt-3"><a class="link-dark" href="/tasks">Cancel</a></p>
              </form>
              
            </div>

           </div>
        </div>
      </div>
    </div>
  </div>
</html>