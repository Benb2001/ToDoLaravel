<!DOCTYPE HTML>
<html>
<head>
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
	<link rel="stylesheet" href="Styles/styles.css">
	<title>To-Do Application</title>
</head>
  <div class="container pt-5 h-100">
    <div class="row d-flex justify-content-center align-items-center h-100">
      <div class="col-12 col-md-8 col-lg-6 col-xl-5">
        <div class="card bg-info text-black" style="border-radius: 2rem;">
          <div class="card-body p-5 text-center">

            <div class="mt-4 pb-5">

              <h2 class="fw-bold mb-5">CREATE AN ACCOUNT</h2>
              <p class="text-black mb-5">Enter a username and password</p>
              <form action="create" method="POST">
                @csrf
                <div class="mb-5 form-floating">
                    <input type="text" name="username" id="txtUsername" class="form-control" placeholder=" " />   
                    <label class="form-label" for="txtUsername">Username:</label>                       
                </div>

                <div class=" mb-5 form-floating">                
                    <input type="password" name="password" id="txtPassword" class="form-control" placeholder=" " />
                    <label class="form-label" for="txtPassword">Password:</label>
                </div>

                <div class=" mb-5 form-floating">                
                    <input type="password" name="confpassword" id="txtConfirmPassword" class="form-control" placeholder=" " />
                    <label class="form-label" for="txtConfirmPassword">Confirm Password:</label>
                </div>

                <button class="btn btn-lg mb-3 px-5 btn btn-success" type="submit">Create Account</button>
                <p class="lead fw-bold mt-3"><a class="link-dark" href="/">Login</a></p>
              </form>
              
            </div>

           </div>
        </div>
      </div>
    </div>
  </div>
</html>