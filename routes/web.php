<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\TaskController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::post("username",[LoginController::class,'userLogin']);

Route::post("create",[LoginController::class,'createAccount']);


Route::post("task",[TaskController::class,'addTask']);

Route::post("delete/{TaskID}",[TaskController::class,'deleteTask']);

Route::get('task-delete/{id}',[TaskController::class,'delete'])->name('task.delete');

Route::get('task-edit/{id}',[TaskController::class,'editview'])->name('task.editview');

Route::post('update/{id}',[TaskController::class,'edit'])->name('task.update');

Route::get('finish/{id}',[TaskController::class,'finish'])->name('task.finish');

Route::get("tasks",[TaskController::class,'show'])->name('tasks');


Route::delete('/tasks/{TaskID}',[TaskController::class,'destroy']);



Route::get("user",[LoginController::class,'userLogin']);

Route::view("create",'createaccount');

Route::view("edit",'edittask')->name('edit');

Route::get("/logout",function()
    {
        if(session()->has('username'))
        {
            session()->pull('username');
        }
        return redirect('/');
    });

Route::get("/",function()
    {
        if(session()->has('username'))
        {
            return redirect('tasks');
        }
        return view('login');
    });



