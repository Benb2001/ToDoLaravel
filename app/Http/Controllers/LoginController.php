<?php

namespace App\Http\Controllers;
use DB;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Blade;
use App\Models\User;
use App\Models\Task;

class LoginController extends Controller
{
    
    function userLogin(Request $req) {
        $input = $req->input();
        $username = $req->input('username');
        $password = $req->input('password');


        $userExists = DB::table('users')->where('username', '=', $req->input('username'))->exists();


        
        $userdata = User::where('username',$username) -> first();
        $passdata = $userdata -> Password;
        
        
        if ($userExists) {
            if($passdata == $password){
                $req->session()->put('username',$input['username']);
                        return redirect('tasks');
                } 
                else {
                        return redirect('/');                
                }
        }
        
        
        
    }

    function createAccount(Request $Req){
            
        $userExists = DB::table('users')->where('username', '=', $Req->input('username'))->exists();
        $username = $Req->input('username');
        $password = $Req->input('password');
        $confpassword = $Req->input('confpassword');


        if ($userExists) {
            return redirect('/create');
         } 
         else {
            if($password == $confpassword){

                $data = Task::all();

                $user = new User;
                $user -> Username = $username;
                $user -> Password = $password;                      
                $user -> save();
                $Req->session()->put('username',$username);
                    
                return view('tasks',['tasks'=>$data]);
            }
            else{
                return redirect('/create');
            }
        }    
              
    }

        
}
