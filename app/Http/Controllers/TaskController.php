<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Task;

class TaskController extends Controller
{
    function show() {

        $data = Task::all();

        if(session()->has('username'))
        {  
            return view('tasks',['tasks'=>$data]);
        }
        return redirect('/');


        
    }

    function addTask(Request $Req){
            
            $task = new Task;
            $task -> TaskDesc = $Req->input('taskdesc');
            $task -> Username = session('username'); 
            $task -> Date = date("Y/m/d");
                       
            $task -> save();
            $data = Task::all();
            $users = User::all();

            return view('tasks',['tasks'=>$data]);
    }
    

    public function delete(Request $Req){
        $data = Task::all();
        $id = $Req->id;     
        Task::where('TaskID', $id)->delete();
        return redirect()->route('tasks');
      }

      public function edit(Request $Req){
        

        
        $id = $Req->id;     
        $thistask = Task::where('TaskID', $id)->first();
        $thistask -> TaskDesc = $Req->input('TaskDesc');
        $thistask -> Date = date("Y/m/d");
        $thistask -> save();

        return redirect()->route('tasks');
      }

      public function editview(Request $Req){

        $id = $Req->id;      
        $dataarray = Task::where('TaskID', $id)->get();
        $data = $dataarray[0];
        return redirect()->route('edit')->with(['data' => $data]);
        
      }


      public function finish(Request $Req){
        

        
        $id = $Req->id;     
        $thistask = Task::where('TaskID', $id)->first();
        if($thistask -> Completed == "No"){
          $thistask -> Completed = "Yes";
        }
        else{
          $thistask -> Completed = "No";
        }
     
        $thistask -> save();
        $data = Task::all();

        return redirect()->route('tasks',['tasks'=>$data]);
      }
    
}



